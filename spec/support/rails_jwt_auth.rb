module RailsJwtAuthAcceptanceSpecHelpers
  include Warden::Test::Helpers

  def sign_in(resource_or_scope, resource = nil)
    resource ||= resource_or_scope
    scope = RailsJwtAuth::Mapping.find_scope!(resource_or_scope)
    login_as(resource, scope: scope)
  end

  def sign_out(resource_or_scope)
    scope = RailsJwtAuth::Mapping.find_scope!(resource_or_scope)
    logout(scope)
  end
end
RSpec.configure do |config|
  config.include RailsJwtAuth::SpecHelpers, type: :controller
  config.include RailsJwtAuthAcceptanceSpecHelpers, type: :acceptance
end
