RSpec.shared_context 'with authenticated member', shared_context: :metadata do
  before do
    sign_in User.find_by(email: 'user@example.com')
  end
end

RSpec.shared_context 'with authenticated admin', shared_context: :metadata do
  before do
    sign_in User.find_by(email: 'admin@example.com')
  end
end
