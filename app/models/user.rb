class User < ApplicationRecord
  include RailsJwtAuth::Authenticatable
  include RailsJwtAuth::Confirmable
  include RailsJwtAuth::Recoverable
  # include RailsJwtAuth::Trackable
  # include RailsJwtAuth::Invitable
  # include RailsJwtAuth::Lockable

  validates :email, presence:   true,
                    uniqueness: true,
                    format:     URI::MailTo::EMAIL_REGEXP

  scope :admins, -> { where(role: 'admin') }
  scope :users, -> { where(role: 'user') }

  def admin?
    role == 'admin'
  end

  def user?
    role == 'user'
  end
end
