module Member
  class MemberApplicationController < ApplicationController
    authenticate!

    private

    def policy_scope(scope)
      super([:member, scope])
    end

    def authorize(record, query = nil)
      super([:member, record], query)
    end
  end
end
